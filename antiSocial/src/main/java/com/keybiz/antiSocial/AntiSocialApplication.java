package com.keybiz.antiSocial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntiSocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(AntiSocialApplication.class, args);
	}

}

