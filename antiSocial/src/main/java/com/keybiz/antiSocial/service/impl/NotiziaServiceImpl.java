package com.keybiz.antiSocial.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keybiz.antiSocial.dto.NotiziaDto;
import com.keybiz.antiSocial.model.Notizia;
import com.keybiz.antiSocial.model.Utente;
import com.keybiz.antiSocial.repository.NotiziaRepository;
import com.keybiz.antiSocial.repository.UtenteRepository;
import com.keybiz.antiSocial.service.NotiziaService;


@Service
public class NotiziaServiceImpl implements NotiziaService{

	
	private final UtenteRepository utenteRepository;
	private final NotiziaRepository notiziaRepository;
	
	@Autowired
	public NotiziaServiceImpl(UtenteRepository utenteRepository, NotiziaRepository notiziaRepository) {
		this.utenteRepository = utenteRepository;
		this.notiziaRepository = notiziaRepository;
	}

	@Override
	public Notizia nuovaNotizia(Long idAutore,String titolo,String testo,String immagine, String sottotitolo, String riassunto, String importanza) {
		Notizia nuovaNotizia = new Notizia();
		Utente autoreNotizia = utenteRepository.getOne(idAutore);
		nuovaNotizia.setAutoreNotizia(autoreNotizia);
		nuovaNotizia.setSottotitolo(sottotitolo);
		nuovaNotizia.setRiassunto(riassunto);
		nuovaNotizia.setTitolo(titolo);
		nuovaNotizia.setTesto(testo);
		nuovaNotizia.setImmagine(immagine);
		nuovaNotizia.setDataCreazione(new Date());
		nuovaNotizia.setDataPubblicazione(null);
		nuovaNotizia.setDataFineVisual(null);
		nuovaNotizia.setImportanza(importanza);
		notiziaRepository.save(nuovaNotizia);
		return nuovaNotizia;
	}

	@Override
	public Collection<NotiziaDto> allNotizie() {
		return notiziaRepository.findAll().stream().map(NotiziaDto::convert).collect(Collectors.toList());
	}

	@Override
	public Optional<Notizia> notiziaById(Long id) {
		return notiziaRepository.findById(id);
	}

	@Override
	public void deleteNotizia(Long id) {
		notiziaRepository.deleteById(id);
	}


	



	


}
