package com.keybiz.antiSocial.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keybiz.antiSocial.dto.ArticoloDto;
import com.keybiz.antiSocial.model.Articolo;
import com.keybiz.antiSocial.model.Utente;
import com.keybiz.antiSocial.repository.ArticoloRepository;
import com.keybiz.antiSocial.repository.UtenteRepository;
import com.keybiz.antiSocial.service.ArticoloService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor=@__({@Autowired}))
public class ArticoloServiceImpl implements ArticoloService {

	private final UtenteRepository utenteRepository;
	private final ArticoloRepository articoloRepository;
	
	@Override
	public Articolo nuovoArticolo(Long idAutore, String titolo, String testo, String sezione) {
		Articolo nuovoArticolo = new Articolo();
		Utente autoreArticolo = utenteRepository.getOne(idAutore);
		nuovoArticolo.setAutoreArticolo(autoreArticolo);
		nuovoArticolo.setDataCreazione(new Date());
		nuovoArticolo.setDataPubblicazione(null);
		nuovoArticolo.setSezione(sezione);
		nuovoArticolo.setTesto(testo);
		nuovoArticolo.setTitolo(titolo);
		articoloRepository.save(nuovoArticolo);
		return nuovoArticolo;
	}

	@Override
	public Collection<ArticoloDto> allArticoli() {
		return articoloRepository.findAll().stream().map(ArticoloDto::convert).collect(Collectors.toList());
	}

	@Override
	public Optional<Articolo> articoloById(Long id) {
		return articoloRepository.findById(id);
	}

	@Override
	public void deleteArticolo(Long id) {
		articoloRepository.deleteById(id);
	}




	
}
