package com.keybiz.antiSocial.service;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import com.keybiz.antiSocial.dto.UtenteDto;
import com.keybiz.antiSocial.model.Utente;

public interface UtenteService {

	public Utente nuovoUtente(String firstname, String lastname, String username, String password,
			Date dob, Double contoVirtuale,String privileges);
	public Collection<UtenteDto> findAll();
	public Optional<Utente> findById(Long id);
	public void deleteUtente(Long id);
}
