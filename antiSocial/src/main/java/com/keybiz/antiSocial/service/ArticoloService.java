package com.keybiz.antiSocial.service;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiSocial.dto.ArticoloDto;
import com.keybiz.antiSocial.model.Articolo;

public interface ArticoloService {
	
	public Articolo nuovoArticolo(Long idAutore, String titolo, String testo, String sezione);
	public Collection<ArticoloDto> allArticoli();
	public Optional<Articolo> articoloById(Long id);
	public void deleteArticolo(Long id);
}
