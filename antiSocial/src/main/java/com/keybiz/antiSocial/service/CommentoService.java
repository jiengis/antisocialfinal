package com.keybiz.antiSocial.service;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiSocial.dto.CommentoDto;
import com.keybiz.antiSocial.model.Commento;

public interface CommentoService {

	public Commento nuovoCommento(Long idAutore, Long idNotizia,String testo);
	public Collection<CommentoDto> findAll();
	public Optional<Commento> findById(Long id);
	public void deleteCommento(Long id);
	
}
