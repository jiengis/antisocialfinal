package com.keybiz.antiSocial.service;


import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiSocial.dto.NotiziaDto;
import com.keybiz.antiSocial.model.Notizia;

public interface NotiziaService {

	public Notizia nuovaNotizia(Long idAutore,String sottotitolo,String titolo,String testo,String immagine, String riassunto,String importanza);
	public Collection<NotiziaDto> allNotizie();
	public Optional<Notizia> notiziaById(Long id);
	public void deleteNotizia(Long id);
}
