package com.keybiz.antiSocial.service.impl;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keybiz.antiSocial.dto.CommentoDto;
import com.keybiz.antiSocial.model.Commento;
import com.keybiz.antiSocial.model.Notizia;
import com.keybiz.antiSocial.model.Utente;
import com.keybiz.antiSocial.repository.CommentoRepository;
import com.keybiz.antiSocial.repository.NotiziaRepository;
import com.keybiz.antiSocial.repository.UtenteRepository;
import com.keybiz.antiSocial.service.CommentoService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor=@__({@Autowired}))
public class CommentoServiceImpl implements CommentoService {

	private final UtenteRepository utenteRepository;
	private final CommentoRepository commentoRepository;
	private final NotiziaRepository notiziaRepository;
	
	@Override
	public Commento nuovoCommento(Long idAutore, Long idNotizia, String testo) {
		Commento commento = new Commento();
		Utente commentatore = utenteRepository.getOne(idAutore);
		Notizia notiziaCommentata = notiziaRepository.getOne(idNotizia);
		commento.setCommentatore(commentatore);
		commento.setNotiziaComm(notiziaCommentata);
		commento.setTesto(testo);
		commentoRepository.save(commento);
		return commento;
	}

	@Override
	public Collection<CommentoDto> findAll() {
		return commentoRepository.findAll().stream().map(CommentoDto::convert).collect(Collectors.toList());
	}

	@Override
	public Optional<Commento> findById(Long id) {
		return commentoRepository.findById(id);
	}

	@Override
	public void deleteCommento(Long id) {
		commentoRepository.deleteById(id);
	}

}
