package com.keybiz.antiSocial.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keybiz.antiSocial.dto.UtenteDto;
import com.keybiz.antiSocial.model.Utente;
import com.keybiz.antiSocial.repository.UtenteRepository;
import com.keybiz.antiSocial.service.UtenteService;


@Service
public class UtenteServiceImpl implements UtenteService {

	private final UtenteRepository utenteRepository;
	
	@Autowired
	public UtenteServiceImpl(UtenteRepository utenteRepository) {
		this.utenteRepository = utenteRepository;
	}
	
	@Override
	public Utente nuovoUtente(String firstname, String lastname, String username, String password, Date dob,
			Double contoVirtuale, String privileges) {
		Utente utente = new Utente();
		utente.setDob(dob);
		utente.setFirstname(firstname);
		utente.setLastname(lastname);
		utente.setUsername(username);
		utente.setPassword(password);
		utente.setContoVirtuale(contoVirtuale);
		utente.setPrivileges(privileges);
		utenteRepository.save(utente);
		return utente;
	}

	@Override
	public Collection<UtenteDto> findAll() {
		return utenteRepository.findAll().stream().map(UtenteDto::convert).collect(Collectors.toList());
	}
	
	@Override
	public Optional<Utente> findById(Long id) {
		return utenteRepository.findById(id);
	}
	
	@Override
	public void deleteUtente(Long id) {
		utenteRepository.deleteById(id);
	}
	


}
