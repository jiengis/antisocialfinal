package com.keybiz.antiSocial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.keybiz.antiSocial.model.Articolo;

public interface ArticoloRepository extends JpaRepository<Articolo, Long> {

}
