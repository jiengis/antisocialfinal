package com.keybiz.antiSocial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.keybiz.antiSocial.model.Commento;

public interface CommentoRepository extends JpaRepository<Commento, Long> {

}
