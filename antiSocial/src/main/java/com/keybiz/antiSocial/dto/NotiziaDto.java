package com.keybiz.antiSocial.dto;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import com.keybiz.antiSocial.model.Notizia;

import lombok.Data;


@Data
public class NotiziaDto {

	private Long id;
	private String titolo;
	private String testo;
	private String immagine;
	private Date dataCreazione;
	private Date dataPubblicazione;
	private String sottotitolo;
	private String riassunto;
	private Date dataFineVisual;
	private String importanza;
	private Long autoreNotizia;
	private Collection<Long> commentiNotizia;
	
	public static NotiziaDto convert(Notizia notizia) {
		NotiziaDto notiziaDto = new NotiziaDto();
		notiziaDto.setId(notizia.getId());
		notiziaDto.setTitolo(notizia.getTitolo());
		notiziaDto.setTesto(notizia.getTesto());
		notiziaDto.setImmagine(notizia.getImmagine());
		notiziaDto.setDataCreazione(notizia.getDataCreazione());
		notiziaDto.setDataPubblicazione(notizia.getDataPubblicazione());
		notiziaDto.setSottotitolo(notizia.getSottotitolo());
		notiziaDto.setRiassunto(notizia.getRiassunto());
		notiziaDto.setDataFineVisual(notizia.getDataFineVisual());
		notiziaDto.setImportanza(notizia.getImportanza());
		notiziaDto.setAutoreNotizia(notizia.getAutoreNotizia().getId());
		notiziaDto.setCommentiNotizia(notizia.getCommentiNotizia().stream().map(c -> c.getId()).collect(Collectors.toList()));
		return notiziaDto;
	}
}
