package com.keybiz.antiSocial.dto;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import com.keybiz.antiSocial.model.Utente;

import lombok.Data;

@Data
public class UtenteDto {


	private Long id;
	private String firstname;
	private String lastname;
	private String username;
	private String password;
	private Date dob;
	private String privileges;
	private Double contoVirtuale;
	private Collection<Long> articoliScritti;
	private Collection<Long> notizieScritte;
	private Collection<Long> commentiScritti;

	public static UtenteDto convert(Utente utente) {
		UtenteDto utenteDto= new UtenteDto();
		utenteDto.setId(utente.getId());
	    utenteDto.setFirstname(utente.getFirstname());
	    utenteDto.setLastname(utente.getLastname());
	    utenteDto.setUsername(utente.getUsername());
	    utenteDto.setPassword(utente.getPassword());
	    utenteDto.setDob(utente.getDob());
	    utenteDto.setPrivileges(utente.getPrivileges());
	    utenteDto.setContoVirtuale(utente.getContoVirtuale());
		utenteDto.setArticoliScritti(utente.getArticoliScritti().stream().map(c -> c.getId()).collect(Collectors.toList()));
	    utenteDto.setNotizieScritte(utente.getNotizieScritte().stream().map(c -> c.getId()).collect(Collectors.toList()));
	    utenteDto.setCommentiScritti(utente.getCommentiScritti().stream().map(c -> c.getId()).collect(Collectors.toList()));
		return utenteDto;
	}
}
