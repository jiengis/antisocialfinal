package com.keybiz.antiSocial.dto;


import java.util.Date;

import com.keybiz.antiSocial.model.Articolo;

import lombok.Data;

@Data
public class ArticoloDto {

	private Long id;
	private String titolo;
	private String testo;
	private String immagine;
	private Date dataCreazione;
	private Date dataPubblicazione;
	private String sezione;
	private Double prezzo;
	private boolean vendibile;
	private Long autoreArticolo;
	
	
	public static ArticoloDto convert(Articolo articolo) {
		ArticoloDto articoloDto = new ArticoloDto();
		articoloDto.setId(articolo.getId());
		articoloDto.setTitolo(articolo.getTitolo());
		articoloDto.setTesto(articolo.getTesto());
		articoloDto.setImmagine(articolo.getImmagine());
		articoloDto.setDataCreazione(articolo.getDataCreazione());
		articoloDto.setDataPubblicazione(articolo.getDataPubblicazione());
		articoloDto.setSezione(articolo.getSezione());
		articoloDto.setPrezzo(articolo.getPrezzo());
		articoloDto.setVendibile(articolo.isVendibile());
		articoloDto.setAutoreArticolo(articolo.getAutoreArticolo().getId());
		return articoloDto;
	}
	
	
}
