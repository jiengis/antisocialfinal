package com.keybiz.antiSocial.dto;

import com.keybiz.antiSocial.model.Commento;

import lombok.Data;

@Data
public class CommentoDto {

	
	private Long id;
	private String testo;
	private Long commentatore;
	private Long notiziaComm;
	
	public static CommentoDto convert(Commento commento) {
		CommentoDto commentoDto = new CommentoDto();
		commentoDto.setId(commento.getId());
		commentoDto.setTesto(commento.getTesto());
		commentoDto.setCommentatore(commento.getCommentatore().getId());
		commentoDto.setNotiziaComm(commento.getNotiziaComm().getId());
		return commentoDto;
		
	}
}
