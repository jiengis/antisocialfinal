package com.keybiz.antiSocial.controller;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keybiz.antiSocial.dto.ArticoloDto;
import com.keybiz.antiSocial.model.Articolo;
import com.keybiz.antiSocial.service.ArticoloService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class ArticoloController {

	private final ArticoloService articoloService;

	@Autowired
	public ArticoloController(ArticoloService articoloService) {
		this.articoloService = articoloService;
	}

	
	@PostMapping("/articoli")
	public ArticoloDto newArticolo(@RequestBody ArticoloDto articoloIn) {
		log.debug("/articolo newArticolo");
		Articolo nuovoArticolo = articoloService.nuovoArticolo(
						articoloIn.getAutoreArticolo(), 
						articoloIn.getTitolo(), 
						articoloIn.getTesto(), 
						articoloIn.getSezione()
						);
		ArticoloDto articoloOut = ArticoloDto.convert(nuovoArticolo);
		return articoloOut;
	}
	
	
	
	@GetMapping("/articoli")
	public Collection<ArticoloDto> getArticoli() {
		log.debug("/articoli findAll");
		return articoloService.allArticoli();
	}

	@GetMapping("/articoli/{id}")
	public ResponseEntity<?> retrieveById(@PathVariable Long id) {
		log.debug("/articolo retrieveById");
		Optional<Articolo> optArticolo = articoloService.articoloById(id);
		if(optArticolo.isPresent()) {
			return ResponseEntity.ok(ArticoloDto.convert(optArticolo.get()));
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	
	
	@DeleteMapping("/articoli/{id}")
	public ResponseEntity<?> deleteArticoloById(@PathVariable Long id) {
		log.debug("/articolo/{id} deleteArticolo");
		if (articoloService.articoloById(id) != null) {
			articoloService.deleteArticolo(id);
			return ResponseEntity.ok(HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

}
