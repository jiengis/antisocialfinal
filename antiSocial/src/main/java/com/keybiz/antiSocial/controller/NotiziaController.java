package com.keybiz.antiSocial.controller;




import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keybiz.antiSocial.dto.NotiziaDto;
import com.keybiz.antiSocial.model.Notizia;
import com.keybiz.antiSocial.service.NotiziaService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class NotiziaController {

	private final NotiziaService notiziaService;
	
	@Autowired
	public NotiziaController(NotiziaService notiziaService) {
		this.notiziaService = notiziaService;
	}
	
	
	@PostMapping("/notizie")
	public NotiziaDto newNotizia(@RequestBody NotiziaDto notiziaDto) {
		log.debug("/notizie newNotizia");
		Notizia nuovaNotizia = notiziaService.nuovaNotizia(
				notiziaDto.getAutoreNotizia(),
				notiziaDto.getSottotitolo(),
				notiziaDto.getRiassunto(),
				notiziaDto.getTitolo(),
				notiziaDto.getTesto(),
				notiziaDto.getImmagine(),
				notiziaDto.getImportanza()
				);
		NotiziaDto notiziaOut = NotiziaDto.convert(nuovaNotizia);
		return notiziaOut;
	}
	
	
	
	@GetMapping("/notizie")
	public Collection<NotiziaDto> getNotizie(){
		log.debug("/notizie getNotizie");
		return notiziaService.allNotizie();
	}

	
	
	@GetMapping("/notizie/{id}")
	public ResponseEntity<?> retrieveById(@PathVariable Long id){
		log.debug("/notizia getNotiziaById");
		Optional<Notizia> optNotizia = notiziaService.notiziaById(id);
		if(optNotizia.isPresent()) {
			return ResponseEntity.ok(NotiziaDto.convert(optNotizia.get()));
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	
	@DeleteMapping("/notizie/{id}")
	public ResponseEntity<?> deleteNotiziaById(@PathVariable Long id){
		log.debug("/notizia/{id} deleteNotizia");
		
		if(notiziaService.notiziaById(id) != null) {
			notiziaService.deleteNotizia(id);
			return ResponseEntity.ok(HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	


}
