package com.keybiz.antiSocial.controller;


import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keybiz.antiSocial.dto.UtenteDto;
import com.keybiz.antiSocial.model.Utente;
import com.keybiz.antiSocial.service.UtenteService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class UtenteController {

	private final UtenteService utenteService;
	
	@Autowired
	public UtenteController(UtenteService utenteService) {
		this.utenteService = utenteService;
	}
	
	
	@PostMapping("/utenti")
	public UtenteDto newUtente(@RequestBody UtenteDto utenteDto) {
		log.debug("/utenti newUtente");
		Utente newUtente = utenteService.nuovoUtente(
				utenteDto.getFirstname(), 
				utenteDto.getLastname(), 
				utenteDto.getUsername(), 
				utenteDto.getPassword(), 
				utenteDto.getDob(),  
				utenteDto.getContoVirtuale(),
				utenteDto.getPrivileges()
				);
		UtenteDto utenteOut = UtenteDto.convert(newUtente);
		return utenteOut;
	}
	
	@GetMapping("/utenti")
	public Collection<UtenteDto> getUtenti(){
		log.debug("/notizie getUtenti");
		return utenteService.findAll();
	}

	@GetMapping("/utenti/{id}")
	public ResponseEntity<?> retrieveById(@PathVariable Long id){
		log.debug("/utente retrieveById");
		Optional<Utente> optUtente = utenteService.findById(id);
		if(optUtente.isPresent()) {
			return ResponseEntity.ok(UtenteDto.convert(optUtente.get()));
		}else{
			return ResponseEntity.notFound().build();	
		}
	}
	
	@DeleteMapping("/utenti/{id}")
	public ResponseEntity<?> deleteUtenteById(@PathVariable Long id){
		log.debug("/utente deleteUtenteById");
		if(utenteService.findById(id) != null ) {
			utenteService.deleteUtente(id);
			return ResponseEntity.ok(HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	
	
	
}





