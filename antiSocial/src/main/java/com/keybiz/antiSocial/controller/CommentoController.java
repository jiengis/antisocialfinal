package com.keybiz.antiSocial.controller;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.keybiz.antiSocial.dto.CommentoDto;
import com.keybiz.antiSocial.model.Commento;
import com.keybiz.antiSocial.service.CommentoService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
public class CommentoController {

	private final CommentoService commentoService;

	@Autowired
	public CommentoController(CommentoService commentoService) {
		this.commentoService = commentoService;
	}

	@PostMapping("/commenti")
	public CommentoDto newCommento(@RequestBody CommentoDto commentoDto) {
		log.debug("/commenti newCommento");
		Commento commentoIn = commentoService.nuovoCommento(
				commentoDto.getCommentatore(),
				commentoDto.getNotiziaComm(), 
				commentoDto.getTesto());
		CommentoDto commentoOut = CommentoDto.convert(commentoIn);
		return commentoOut;
	}
	
	@GetMapping("/commenti")
	public Collection<CommentoDto> retrieveAll() {
		log.debug("/commenti retrieveAll");
		return commentoService.findAll();
	}
	
	@GetMapping("/commenti/{id}")
	public ResponseEntity<?> commentoById(@PathVariable Long id){
		log.debug("/commenti/{id} commentoById");
		Optional<Commento> optCommento = commentoService.findById(id);
		if(optCommento.isPresent()) {
			return ResponseEntity.ok(CommentoDto.convert(optCommento.get()));
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@DeleteMapping("/commenti/{id}")
	public ResponseEntity<?> deleteById(@PathVariable Long id){
		log.debug("/commenti/{id}");
		if(commentoService.findById(id) != null ) {
			commentoService.deleteCommento(id);
			return ResponseEntity.ok(HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

}
