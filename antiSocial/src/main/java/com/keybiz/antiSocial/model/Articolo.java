package com.keybiz.antiSocial.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Articolo extends AbstractModelContenuto {

	
	private String sezione;
	private Double prezzo;
	private boolean vendibile;
	
	@ManyToOne
	private Utente autoreArticolo;
}








