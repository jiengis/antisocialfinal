package com.keybiz.antiSocial.model;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


import lombok.Data;

@Data
@MappedSuperclass
public abstract class AbstractModelContenuto {

	@Id
	@GeneratedValue
	private Long id;

	private String titolo;
	private String testo;
	private String immagine;
	private Date dataCreazione;
	private Date dataPubblicazione;
}
