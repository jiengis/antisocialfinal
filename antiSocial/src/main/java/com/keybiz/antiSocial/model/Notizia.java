package com.keybiz.antiSocial.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Notizia extends AbstractModelContenuto {

	
	private String sottotitolo;
	private String riassunto;
	private Date dataFineVisual;
	private String importanza;
	
	@ManyToOne
	private Utente autoreNotizia;
	
	@OneToMany(mappedBy = "notiziaComm")
	private Collection<Commento> commentiNotizia;
}
